package models;

import models.contracts.Boards;
import models.contracts.Bug;
import models.contracts.WorkItems;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class BoardsImplTest {

    private Boards testBoard;
    private String testHistory;
    private WorkItems testWorkItem;

    @Before
    public void before(){
        testBoard=new BoardsImpl("Name");
        testHistory="Test";
        testWorkItem=new BugImpl("BugImplName","Test for boards");
    }

    @Test(expected = NullPointerException.class)
    public void constructor_should_throw_when_nameIsNull(){
        //Act
        Boards board = new BoardsImpl(null);
    }
    @Test
    public void check_add_history(){
        //arrange
        testBoard.addToHistory(testHistory);
        Assert.assertEquals(1,testBoard.getHistory().size());
    }

    @Test
    public void check_add_workItem(){
        testBoard.addWorkItem(testWorkItem);
        Assert.assertEquals(1,testBoard.getListOfWorkingItems().size());
    }





}