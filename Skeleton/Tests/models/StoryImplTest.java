package models;


import models.contracts.Story;
import org.junit.Test;

public class StoryImplTest {

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throw_when_titleLessThanMinValue(){
        Story story = new StoryImpl("sda","sadjsdkagsajdsadsad dsasdsa dasd");
        //Act
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throw_when_titleMoreThanMaxValue(){
        //Act
        Story story = new StoryImpl("dsadasdasddsadasdasddsadasdasddsadasdasddsadasdasddsadasdasd", "desascasjk sdasd sda");
    }

    @Test(expected = NullPointerException.class)
    public void constructor_should_throw_when_titleIsNull(){
        //Act
        Story story = new StoryImpl(null,"dasdasdsad sajkdhakshd asdha");
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throw_when_descriptionLessThanMinValue(){
        //Act
        Story story = new StoryImpl("ProblemSolved","ddd");
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throw_when_descriptionMoreThanMaxValue(){
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i <= 502 ; i++) {
            String test = "text";
            stringBuilder.append(test);
        }
        //Act
        Story story = new StoryImpl("ProblemSolved",stringBuilder.toString());
    }

    @Test(expected = NullPointerException.class)
    public void constructor_should_throw_when_descriptionIsNull(){
        //Act
        Story story = new StoryImpl("ProblemSolved",null);
    }


}