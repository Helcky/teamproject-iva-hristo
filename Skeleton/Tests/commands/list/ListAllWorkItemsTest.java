package commands.list;

import commands.contracts.Command;
import core.ItemRepositoryImpl;
import core.contracts.ItemRepository;
import models.BugImpl;
import models.StoryImpl;
import models.contracts.Bug;
import models.contracts.Story;
import models.contracts.WorkItems;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ListAllWorkItemsTest {

    private ItemRepository itemRepository;
    private Command testCommand;
    private Bug testBug;
    private Story testStory;
    private List<WorkItems> testShow;

    @Before
    public void before() {
        itemRepository = new ItemRepositoryImpl();
        testCommand = new ListAllWorkItems(itemRepository);
        testBug = new BugImpl("bugNameTest", "descriptionTest");
        testStory = new StoryImpl("storyNameTest", "descriptionTest");
        testShow = new ArrayList<>();
        itemRepository.addItems(testBug.getTitle(), testBug);
        itemRepository.addItems(testStory.getTitle(), testStory);
        testShow.add(testBug);
        testShow.add(testStory);
    }

    @Test
    public void execute_should_ListAllWorkItems() {
        //Arrange
        List<String> testList = new ArrayList<>();
        List<WorkItems> testShowAfter = new ArrayList<>(itemRepository.getItems().values());
        //Act
        testCommand.execute(testList);
        //Assert
        Assert.assertEquals(testShow.toString(), testShowAfter.toString());
    }
}