package commands.list;

import commands.contracts.Command;
import core.ItemRepositoryImpl;
import core.contracts.ItemRepository;
import models.FeedbackImpl;
import models.contracts.Feedback;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


public class FilterFeedbackTest {

    private ItemRepository itemRepository;
    private Command testCommand;
    private Feedback testFeedback;
    private List<Feedback> testShow;

    @Before
    public void before() {
        itemRepository = new ItemRepositoryImpl();
        testCommand = new FilterFeedback(itemRepository);
        testShow = new ArrayList<>();
        testFeedback = new FeedbackImpl("feedbackNameTest", "descriptionTest", 2);
        itemRepository.addFeedback(testFeedback.getTitle(), testFeedback);
        testShow.add(testFeedback);
    }

    @Test
    public void execute_should_showAllFeedback() {
        //Arrange
        List<String> testList = new ArrayList<>();

        List<Feedback> testShowAfter = new ArrayList<>(itemRepository.getFeedbackList().values());
        //Act
        testCommand.execute(testList);
        //Assert
        Assert.assertEquals(testShow.toString(), testShowAfter.toString());
    }
}