package commands.list;

import commands.contracts.Command;
import core.ItemRepositoryImpl;
import core.contracts.ItemRepository;
import models.StoryImpl;
import models.contracts.Story;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class FilterStoriesTest {
    private ItemRepository itemRepository;
    private Story testStory;
    private List<Story> testStoryList;
    private Command testCommand;

    @Before
    public void before() {
        itemRepository = new ItemRepositoryImpl();
        testStory = new StoryImpl("Storytitle", "descriptiondescription");
        itemRepository.addStory(testStory.getTitle(), testStory);
        testCommand = new FilterStories(itemRepository);
        testStoryList = new ArrayList<>();
        testStoryList.add(testStory);
    }

    @Test
    public void execute_should_FilterStories() {
        //Arrange
        List<String> testList = new ArrayList<>();
        List<Story> testStoryListAfter = new ArrayList<Story>(itemRepository.getStoryList().values());
        //Act
        testCommand.execute(testList);
        //Assert
        Assert.assertEquals(testStoryList.toString(), testStoryListAfter.toString());
    }
}