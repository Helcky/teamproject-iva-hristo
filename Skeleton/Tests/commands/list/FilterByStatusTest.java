package commands.list;

import commands.contracts.Command;
import core.ItemRepositoryImpl;
import core.contracts.ItemRepository;
import models.BugImpl;
import models.StoryImpl;
import models.contracts.Bug;
import models.contracts.Story;
import models.contracts.WorkItems;
import models.enums.BugStatusType;
import models.enums.StoryStatusType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


public class FilterByStatusTest {
    private ItemRepository itemRepository;
    private Command testCommand;
    private Bug testBug;
    private Story testStory;
    private List<WorkItems> testShow;

    @Before
    public void before() {
        itemRepository = new ItemRepositoryImpl();
        testCommand = new FilterByStatus(itemRepository);
        testBug = new BugImpl("Titletitle", "descriptiondescription");
        testStory = new StoryImpl("Titletisdsatle", "descriptw231iondescription");
        itemRepository.addItems(testBug.getTitle(), testBug);
        itemRepository.addItems(testStory.getTitle(), testStory);
        testBug.setStatusType(BugStatusType.ACTIVE);
        testStory.setStatus(StoryStatusType.NOTDONE);
        testShow = new ArrayList<>();
        testShow.add(testBug);
        testShow.add(testStory);

    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        //Arrange
        List<String> testList = new ArrayList<>();
        //Act&Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("1111");
        testList.add("2222");
        testList.add("3333");
        testList.add("4444");
        testList.add("5555");
        testList.add("6666");
        testList.add("7777");
        //Act&Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_FilterByStatus() {
        List<String> testList = new ArrayList<>();
        testList.add(String.valueOf(BugStatusType.ACTIVE));

        List<WorkItems> showAfter = new ArrayList<>(itemRepository.getItems().values());

        for (WorkItems item : testShow) {
            if (item.getGlobalStatus().equals(AllStatusEnum.ACTIVE)) {
                showAfter.add(item);
            }
        }
        //Act
        testCommand.execute(testList);
        //Assert
        Assert.assertNotEquals(testShow, showAfter);


    }

}