package commands.list;

import commands.contracts.Command;
import core.ItemRepositoryImpl;
import core.contracts.ItemRepository;
import models.FeedbackImpl;
import models.MembersImpl;
import models.contracts.Feedback;
import models.contracts.Members;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class SortWorkItemByRatingTest {
    private ItemRepository itemRepository;
    private Command testCommand;
    private Feedback testFeedbackFirst;
    private Feedback testFeedbackSecond;
    private List<Feedback>  testShow;

    @Before
    public void before(){
        itemRepository = new ItemRepositoryImpl();
        testCommand = new SortWorkItemByRating(itemRepository);
        testFeedbackFirst = new FeedbackImpl("FirstFirst","DescriptionDescription",3);
        testFeedbackSecond = new FeedbackImpl("SecondSecond","DescriptionDescription",5);
        itemRepository.addFeedback(testFeedbackFirst.getTitle(),testFeedbackFirst);
        itemRepository.addFeedback(testFeedbackSecond.getTitle(),testFeedbackSecond);
        testFeedbackFirst.setRating(3);
        testFeedbackSecond.setRating(5);
        testShow = new ArrayList<>();
        testShow.add(testFeedbackSecond);
        testShow.add(testFeedbackFirst);
    }

    @Test
    public void execute_should_SortWorkitemByRating(){
        //Arrange
        List<String> testList = new ArrayList<>();
        List<Feedback> testShowAfter = new ArrayList<>(itemRepository.getFeedbackList().values());
        //Act
        testCommand.execute(testList);
        //Assert
        Assert.assertEquals(testShow.toString(),testShowAfter.toString());

    }
}