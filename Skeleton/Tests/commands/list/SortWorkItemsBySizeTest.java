package commands.list;

import commands.contracts.Command;
import core.ItemRepositoryImpl;
import core.contracts.ItemRepository;
import models.StoryImpl;
import models.contracts.Story;
import models.enums.SizeType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class SortWorkItemsBySizeTest {
    private ItemRepository itemRepository;
    private Command testCommand;
    private Story testStoryFirst;
    private Story testStorySecond;
    private List<Story> testShow;

    @Before
    public void before(){
        itemRepository = new ItemRepositoryImpl();
        testCommand = new SortWorkItemsBySize(itemRepository);
        testStoryFirst = new StoryImpl("StoryStory","DescritionDescription");
        testStorySecond = new StoryImpl("SecondSecond","DescriptionDescription");
        itemRepository.addStory(testStoryFirst.getTitle(),testStoryFirst);
        itemRepository.addStory(testStorySecond.getTitle(),testStorySecond);
        testStorySecond.setSize(SizeType.MEDIUM);
        testStoryFirst.setSize(SizeType.SMALL);
        testShow = new ArrayList<>();
        testShow.add(testStorySecond);
        testShow.add(testStoryFirst);
    }

    @Test
    public void execute_should_SortWorkitemBySize(){
        //Arrange
        List<String> testList = new ArrayList<>();
        List<Story> testShowAfter = new ArrayList<>(itemRepository.getStoryList().values());
        //Act
        testCommand.execute(testList);
        //Assert
        Assert.assertNotEquals(testShow.toString(),testShowAfter.toString());
    }

}