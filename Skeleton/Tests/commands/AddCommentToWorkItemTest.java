package commands;

import commands.contracts.Command;
import core.ItemRepositoryImpl;
import core.contracts.ItemRepository;
import models.*;
import models.contracts.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class AddCommentToWorkItemTest {

    private ItemRepository itemRepository;
    private Command testCommand;
    private Members testPerson;
    private Boards testBoard;
    private Teams testTeam;
    private WorkItems  testItem;
    private Comment testComment;

    @Before
    public void before(){
        itemRepository=new ItemRepositoryImpl();
        testCommand=new AddCommentToWorkItem(itemRepository);
        testPerson=new MembersImpl("personNameTest");
        testBoard=new BoardsImpl("BoardNameTest");
        testTeam=new TeamsImpl("TeamNameTest");
        testItem=new BugImpl("testBugName","testDescriptionBug");
        testComment=new CommentImpl(testPerson,"Message");
        itemRepository.addMembers(testPerson.getName(),testPerson);
        itemRepository.addBoards(testBoard.getName(),testBoard);
        itemRepository.addTeams(testTeam.getName(),testTeam);
        itemRepository.addItems(testItem.getTitle(),testItem);

    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments(){
        //Arrange
        List<String> testList=new ArrayList<>();
        testList.add("aaaa");
        //Act&Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments(){
        //Arrange
        List<String> testList=new ArrayList<>();
        testList.add("aaaa");
        testList.add("bbbb");
        testList.add("cccc");
        testList.add("cccc");
        testList.add("cccc");
        testList.add("cccc");
        //Act&Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_addCommentToWorkItem_when_inputIsValid(){
        //Arrange
        testTeam.addBoardToTeam(testBoard);
        testTeam.addMemberToTeam(testPerson);
        testBoard.addWorkItem(testItem);

        List<String> testList=new ArrayList<>();
        testList.add(testPerson.getName());
        testList.add(testBoard.getName());
        testList.add(testTeam.getName());
        testList.add(testItem.getTitle());
        testList.add("MessageTest");
        //Act
        testCommand.execute(testList);
        //Assert
        Assert.assertEquals(1,testItem.getComments().size());
    }




}