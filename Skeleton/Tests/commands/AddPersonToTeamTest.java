package commands;

import commands.contracts.Command;
import core.ItemRepositoryImpl;
import core.contracts.ItemRepository;
import models.MembersImpl;
import models.TeamsImpl;
import models.contracts.Members;
import models.contracts.Teams;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class AddPersonToTeamTest {

    private ItemRepository itemRepository;
    private Command testCommand;
    private Members testPerson;
    private Teams testTeam;

    @Before
    public void before(){
        itemRepository=new ItemRepositoryImpl();
        testCommand=new AddPersonToTeam(itemRepository);
        testPerson=new MembersImpl("personNameTest");
        testTeam=new TeamsImpl("teamNameTest");
        itemRepository.addMembers(testPerson.getName(),testPerson);
        itemRepository.addTeams(testTeam.getName(),testTeam);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments(){
        //Arrange
        List<String> testList=new ArrayList<>();
        testList.add("aaaa");
        //Act&Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments(){
        //Arrange
        List<String> testList=new ArrayList<>();
        testList.add("aaaa");
        testList.add("bbbb");
        testList.add("cccc");
        //Act&Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_addPersonToTeam_when_inputIsValid(){

        //Arrange
        List<String> testList=new ArrayList<>();
        testList.add(testTeam.getName());
        testList.add(testPerson.getName());
        //Act
        testCommand.execute(testList);
        //Assert
        Assert.assertEquals(1,testTeam.getMembers().size());

    }


}