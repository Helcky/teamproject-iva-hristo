package commands;

import commands.contracts.Command;
import core.ItemRepositoryImpl;
import core.contracts.ItemRepository;
import models.BoardsImpl;
import models.MembersImpl;
import models.contracts.Boards;
import models.contracts.Members;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;



public class ShowBoardActivityTest {
    private ItemRepository itemRepository;
    private Boards testBoard;
    private Command testCommand;
    private String testShow;

    @Before
    public void before(){
        itemRepository = new ItemRepositoryImpl();
        testBoard = new BoardsImpl("NiNJARONI");
        testCommand = new ShowBoardActivity(itemRepository);
        itemRepository.addBoards(testBoard.getName(),testBoard);
        testBoard.addToHistory("TESHDS FOR ACTIVI");
        testShow=testBoard.printHistory();
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        //Arrange
        List<String> testList = new ArrayList<>();
        //Act&Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("1111");
        testList.add("2222");
        testList.add("3333");
        testList.add("4444");
        testList.add("5555");
        testList.add("6666");
        testList.add("7777");
        //Act&Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_ShowBoardsActiviry(){
        itemRepository.addBoards(testBoard.getName(),testBoard);
        List<String> testList = new ArrayList<>();
        testList.add(testBoard.getName());

        String newString = itemRepository.getBoard().get(testBoard.getName()).printHistory();

        testCommand.execute(testList);

        Assert.assertEquals(testShow,newString);
    }

}