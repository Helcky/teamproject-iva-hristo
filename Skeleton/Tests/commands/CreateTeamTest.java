package commands;

import commands.contracts.Command;
import core.ItemRepositoryImpl;
import core.contracts.ItemRepository;
import core.contracts.WIMFactory;
import core.factories.WIMFactoryImpl;
import models.TeamsImpl;
import models.contracts.Teams;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CreateTeamTest {
    private ItemRepository itemRepository;
    private WIMFactory wimFactory;
    private Command testCommand;
    private Teams testTeam;

    @Before
    public void before(){
        wimFactory = new WIMFactoryImpl();
        itemRepository = new ItemRepositoryImpl();
        testCommand = new CreateTeam(itemRepository,wimFactory);
        testTeam = new TeamsImpl("NinjaRoni");
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments(){
        //Arrange
        List<String> testList = new ArrayList<>();

        //Act
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments(){
        List<String> testList = new ArrayList<>();
        testList.add("lololol");
        testList.add("lololol");
        testList.add("lololol");

        testCommand.execute(testList);
    }

    @Test
    public void execute_should_createTeam_when_inputIsValid(){
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add(testTeam.getName());

        testCommand.execute(testList);

        Assert.assertEquals(1,itemRepository.getTeams().size());
    }

}