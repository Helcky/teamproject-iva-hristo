package commands;

import commands.contracts.Command;
import core.ItemRepositoryImpl;
import core.contracts.ItemRepository;
import models.MembersImpl;
import models.contracts.Members;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Member;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ShowPeopleActivityTest {

    private ItemRepository itemRepository;
    private Command testCommand;
    private Members testPerson;
    private String testShow;

    @Before
    public void before(){
        itemRepository=new ItemRepositoryImpl();
        testCommand=new ShowPeopleActivity(itemRepository);
        testPerson=new MembersImpl("personNameTest");
        testPerson.addToHistory("Test for activity");
        testShow=testPerson.printHistory();
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments(){
        //Arrange
        List<String> testList=new ArrayList<>();
        //Act&Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments(){
        //Arrange
        List<String> testList=new ArrayList<>();
        testList.add("aaaa");
        testList.add("bbbb");
        //Act&Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_PeopleActivity(){
        //Arrange
        itemRepository.addMembers(testPerson.getName(),testPerson);
        List<String> testList=new ArrayList<>();
        testList.add(testPerson.getName());

        String testSHowAfter=itemRepository.getMembers().get(testPerson.getName()).printHistory();
        //Act
        testCommand.execute(testList);
        //Assert
        Assert.assertEquals(testSHowAfter,testShow);

    }



}