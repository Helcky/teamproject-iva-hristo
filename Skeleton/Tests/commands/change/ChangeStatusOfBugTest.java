package commands.change;

import commands.contracts.Command;
import core.ItemRepositoryImpl;
import core.contracts.ItemRepository;
import models.BoardsImpl;
import models.BugImpl;
import models.MembersImpl;
import models.TeamsImpl;
import models.contracts.Boards;
import models.contracts.Bug;
import models.contracts.Members;
import models.contracts.Teams;
import models.enums.BugStatusType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


public class ChangeStatusOfBugTest {
    private ItemRepository itemRepository;
    private Command testCommand;
    private Members testMember;
    private Boards testBoard;
    private Teams testTeam;
    private Bug testBug;
    private BugStatusType newStatus;

    @Before
    public void before() {
        itemRepository = new ItemRepositoryImpl();
        testCommand = new ChangeStatusOfBug(itemRepository);
        testMember = new MembersImpl("Icaka");
        testBoard = new BoardsImpl("To do list");
        testTeam = new TeamsImpl("NinjaRoni");
        testBug = new BugImpl("Titletitle", "descriptiondescription");
        itemRepository.addMembers(testMember.getName(),testMember);
        itemRepository.addBoards(testBoard.getName(),testBoard);
        itemRepository.addTeams(testTeam.getName(),testTeam);
        itemRepository.addBug(testBug.getTitle(),testBug);
        newStatus = BugStatusType.ACTIVE;

    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("lolo");
        //Act&Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("1111");
        testList.add("2222");
        testList.add("3333");
        testList.add("4444");
        testList.add("5555");
        testList.add("6666");
        testList.add("7777");
        //Act&Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_ChangeStatusOfBug_when_inputIsValid() {
        //Arrange
        testTeam.addMemberToTeam(testMember);
        testTeam.addBoardToTeam(testBoard);
        testBoard.addWorkItem(testBug);
        String currentStatus = testBug.getStatusType().toString();
        List<String> testList = new ArrayList<>();
        testList.add(testMember.getName());
        testList.add(testBoard.getName());
        testList.add(testTeam.getName());
        testList.add(testBug.getTitle());
        testList.add(String.valueOf(newStatus));
        //Act
        testCommand.execute(testList);
        //Assert
        Assert.assertNotEquals(currentStatus, newStatus);
    }
}