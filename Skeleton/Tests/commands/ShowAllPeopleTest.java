package commands;

import commands.contracts.Command;
import core.ItemRepositoryImpl;
import core.contracts.ItemRepository;
import models.MembersImpl;
import models.contracts.Members;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ShowAllPeopleTest {

    private ItemRepository itemRepository;
    private List<String> testShow;
    private Command testCommand;
    private Members testPerson;

    @Before
    public void before(){
        itemRepository=new ItemRepositoryImpl();
        testCommand=new ShowAllPeople(itemRepository);
        testPerson=new MembersImpl("personNameTest");
        testShow=new ArrayList<>();
        testShow.add(testPerson.getName());
    }

    @Test
    public void execute_should_ShowAllPeople(){
        //Arrange
        itemRepository.addMembers(testPerson.getName(),testPerson);
        List<String> testList=new ArrayList<>();
        List<String> testShowAfter=new ArrayList<>(itemRepository.getMembers().keySet());
        //Act
        testCommand.execute(testList);
        //Assert
        Assert.assertEquals(testShow.toString(),testShowAfter.toString());
    }
}