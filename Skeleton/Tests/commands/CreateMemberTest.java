package commands;

import commands.contracts.Command;
import core.ItemRepositoryImpl;
import core.contracts.ItemRepository;
import core.contracts.WIMFactory;
import core.factories.WIMFactoryImpl;
import models.MembersImpl;
import models.contracts.Members;
import models.contracts.WorkItems;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CreateMemberTest {
    private ItemRepository itemRepository;
    private WIMFactory wimFactory;
    private Command testCommand;
    private Members testMember;

    @Before
    public void before(){
        wimFactory = new WIMFactoryImpl();
        itemRepository = new ItemRepositoryImpl();
        testCommand = new CreateMember(wimFactory,itemRepository);
        testMember = new MembersImpl("Ico");
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments(){
        List<String> testList = new ArrayList<>();
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments(){
        List<String> testList = new ArrayList<>();
        testList.add("lololol");
        testList.add("lololol");
        testList.add("lololol");

        testCommand.execute(testList);
    }

    @Test
    public void execute_should_createMember_when_inputIsValid(){
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add(testMember.getName());

        //Act
        testCommand.execute(testList);

        //Assert
        Assert.assertEquals(1,itemRepository.getMembers().size());
    }


}