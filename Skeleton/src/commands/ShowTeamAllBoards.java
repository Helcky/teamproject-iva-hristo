package commands;

import commands.contracts.Command;
import core.contracts.ItemRepository;
import models.contracts.Boards;
import models.contracts.Teams;

import java.util.List;

import static commands.CommandConstants.*;

public class ShowTeamAllBoards implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    public ShowTeamAllBoards(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    private final ItemRepository itemRepository;

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(INVALID_NUMBER_OF_ARGUMENTS);
        }
        String showAllTeamBoards = parameters.get(0);
        return showAllTeamBoards(showAllTeamBoards);
    }

    private String showAllTeamBoards(String teamName) {

        if (!itemRepository.getTeams().containsKey(teamName)) {
            throw new IllegalArgumentException(String.format(TEAM_NOT_EXISTS_ERROR_MESSAGE, teamName));
        }
        Teams team = itemRepository.getTeams().get(teamName);
        List<Boards> board = team.getBoards();

        if (board.size() == 0) {
            return String.format(NO_BOARDS_IN_TEAM, teamName);
        }
        return board.toString();
    }
}
