package commands;


public class CommandConstants {

    // Error messages
    public static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments. Expected: %d, Received: %d";
    static final String PERSON_NOT_EXISTS_ERROR_MESSAGE = "Person with name: %s do not exist";
    static final String MEMBER_EXISTS_ERROR_MESSAGE = "Member with the name: %s already exists";
    static final String BOARD_EXISTS_ERROR_MESSAGE = "Board with the name: %s already exists";
    static final String BOARD_NOT_EXISTS_ERROR_MESSAGE = "Board with name: %s do not exist";
    static final String TEAM_NOT_EXISTS_ERROR_MESSAGE = "Team with name: %s does not exist";
    static final String NOT_EXISTING_TEAM_ERROR_MESSAGE = "Team with name: %s does not exist";
    static final String NOT_EXISTING_PERSON_ERROR_MESSAGE = "Person with name: %s does not exist";
    static final String TEAM_EXISTS_ERROR_MESSAGE = "Team with name %s already exist";
    static final String NO_EXISTING_PEOPLE = "There is no existing people";
    static final String NO_SUCH_TEAM = "There is no such team";
    static final String NO_MEMBERS_IN_TEAM = "There is no members in team %s";
    static final String NO_BOARDS_IN_TEAM = "There is no boards in team %s";
    static final String NO_EXISTING_TEAM = "There is no existing team!";
    static final String NO_EXISTING_PERSON = "Person with name: %s do not exist";
    static final String NO_EXISTING_BOARD = "Board with name %s do not exist";
    static final String NO_EXISTING_BOARDS = "There is no existing boards";
    static final String NOT_EXISTING_ITEM_ERROR_MESSAGE = "Work item with name: %s do not exist";
    static final String NO_EXISTING_PERSON_OR_BOARD_IN_TEAM =
            "No existing person with name: %s or board with name: %s in team with name: %s";
    static final String FAILED_ASSIGNEE = "Command for assignee change failed";
    static final String FAILED_UNASSIGNEE = "Command for unassigne failed";


    // Success messages
    static final String TEAM_CREATED_SUCCESS_MESSAGE = "Team with name: %s created";
    static final String MEMBER_CREATED_SUCCESS_MESSAGE = "Member with name: %s created";
    static final String BUG_CREATED_SUCCESS_MESSAGE = "Bug with title: %s created";
    static final String STORY_CREATED_SUCCESS_MESSAGE = "Story with title: %s created";
    static final String FEEDBACK_CREATED_SUCCESS_MESSAGE = "Feedback with title: %s created";
    static final String BOARD_CREATED_SUCCESS_MESSAGE = "Board with name: %s created";
    static final String PERSON_ADDED_SUCCESS_MESSAGE = "Member with name: %s was added to team %s";
    static final String ADD_STORY_TO_HISTORY_MESSAGE = "%s created story on board with name: %s";
    static final String ADD_FEEDBACK_TO_HISTORY_MESSAGE = "%s created feedback on board with name: %s";
    static final String ADD_BUG_TO_HISTORY_MESSAGE = "%s created bug on board with name: %s";
    static final String ADD_BOARD_TO_HISTORY = "Board with name: %s was created in team: %s";
    static final String ADD_PERSON_TO_HISTORY_MESSAGE = "Person with name: %s was created";
    static final String ADD_PERSON_TO_TEAM_HISTORY_MESSAGE = "Person with name: %s was added to team with name: %s";
    static final String ADDED_COMMENT_TO_WORKITEM = "Added comment: %s";
    static final String COMMENT_ADDED_SUCCESS_MESSAGE = "Person with name: %s added comment to work item: %s";
    static final String SUCCESSFULLY_ASSIGNED_WORK_ITEM = "Work item with name: %s was assigned to person with name: %s";
    static final String SUCCESSFULLY_UNASSIGNED_WORK_ITEM = "Work item with name: %s was unassigned to person with name: %s";
    static final String ADDED_COMMENT_TO_WORKITEM_IN_BOARD = "%s added comment: %s to work item in board: %s";
    static final String WAS_ASSIGNED = "Work item with name: %s was assigned to you";
    static final String WAS_ASSIGNED_TO_BOARD = "Work item with name: %s was assigned to member with name: %s";
    static final String UNASSIGNED_WORKITEM = "Work item with name: %s was unassigned to you";
    static final String UNASSIGNED_WORK_ITEM_TO_BOARD = "Work item with name: %s was unassigned to member with name: %s";

}

