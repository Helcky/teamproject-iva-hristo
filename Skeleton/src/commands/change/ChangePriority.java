package commands.change;

import commands.contracts.Command;
import core.contracts.ItemRepository;
import models.contracts.*;
import models.enums.PriorityType;

import java.util.List;

//import static commands.CommandConstants.*;
//import static commands.CommandConstants.NOT_EXISTING_ITEM_ERROR_MESSAGE;
import static commands.CommandConstants.INVALID_NUMBER_OF_ARGUMENTS;
import static commands.change.ChangeCommandConstant.*;

public class ChangePriority implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 5;

    private ItemRepository itemRepository;

    public ChangePriority(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public String execute(List<String> parameters) {

        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(INVALID_NUMBER_OF_ARGUMENTS);
        }

        if (itemRepository.getStoryList().size() == 0 && itemRepository.getBugList().size() == 0) {
            throw new IllegalArgumentException(EMPTY_LIST_OF_ITEMS);
        }

        String personName=parameters.get(0);
        String boardName=parameters.get(1);
        String teamName=parameters.get(2);
        String itemName = parameters.get(3);
        String priority = parameters.get(4);
        return changePriority(personName,boardName,teamName,itemName, priority);
    }

    private String changePriority(String personName,String boardName, String teamName, String itemName, String priority) {
        PriorityType priorityType = PriorityType.valueOf(priority.toUpperCase());

        if (!itemRepository.getMembers().containsKey(personName)) {
            throw new IllegalArgumentException(String.format(PERSON_NOT_EXISTS_ERROR_MESSAGE, personName));
        }
        if (!itemRepository.getBoard().containsKey(boardName)) {
            throw new IllegalArgumentException(String.format(BOARD_NOT_EXISTS_ERROR_MESSAGE, boardName));
        }
        if(!itemRepository.getTeams().containsKey(teamName)){
            throw new IllegalArgumentException(String.format(TEAM_NOT_EXISTS_ERROR_MESSAGE,teamName));
        }

        Members person=itemRepository.getMembers().get(personName);
        Boards board=itemRepository.getBoard().get(boardName);
        Teams team=itemRepository.getTeams().get(teamName);
        WorkItems item=itemRepository.getItems().get(itemName);

        if (!(team.getMembers().contains(person) && team.getBoards().contains(board))) {
            return String.format(NO_EXISTING_PERSON_OR_BOARD_IN_TEAM, personName, boardName, teamName);
        }

        if(!board.getListOfWorkingItems().contains(item)){
            return String.format(NOT_EXISTING_ITEM_ERROR_MESSAGE,itemName);
        }

        switch (isInstance(itemName)) {
            case 1:
                Bug bug = itemRepository.getBugList().get(itemName);
                bug.setPriorityType(priorityType);
                board.addToHistory(String.format(ADD_TO_BOARD_HISTORY,personName,itemName));
                person.addToHistory(String.format(ADD_TO_PERSON_HISTORY,itemName));
                return String.format(SUCCESSFULLY_CHANGED_PRIORITY,itemName);
            case 2:
                Story story = itemRepository.getStoryList().get(itemName);
                story.setPriorityType(priorityType);
                board.addToHistory(String.format(ADD_TO_BOARD_HISTORY,personName,itemName));
                person.addToHistory(String.format(ADD_TO_PERSON_HISTORY,itemName));
                return String.format(SUCCESSFULLY_CHANGED_PRIORITY,itemName);
            default:
                return String.format(NO_EXISTING_WORK_ITEM, itemName);
        }
    }

    private int isInstance(String nameToCheck) {

        if (itemRepository.getBugList().containsKey(nameToCheck)) {
            return 1;
        } else if (itemRepository.getStoryList().containsKey(nameToCheck)) {
            return 2;
        } else {
            throw new IllegalArgumentException(String.format(NO_EXISTING_WORK_ITEM,nameToCheck));
        }
    }

}
