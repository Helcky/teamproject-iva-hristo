package commands.change;

import commands.contracts.Command;
import core.contracts.ItemRepository;
import models.contracts.Boards;
import models.contracts.Bug;
import models.contracts.Members;
import models.contracts.Teams;
import models.enums.BugStatusType;

import static commands.CommandConstants.INVALID_NUMBER_OF_ARGUMENTS;
import static commands.change.ChangeCommandConstant.*;


import java.util.List;

public class ChangeStatusOfBug implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 5;

    private ItemRepository itemRepository;

    public ChangeStatusOfBug(ItemRepository itemRepository){
        this.itemRepository=itemRepository;
    }

    public String execute(List<String> parameters){
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(INVALID_NUMBER_OF_ARGUMENTS);
        }

        if(itemRepository.getBugList().size()==0){
            throw new IllegalArgumentException(EMPTY_LIST_OF_BUGS);
        }

        String personName=parameters.get(0);
        String boardName=parameters.get(1);
        String teamName=parameters.get(2);
        String bugName=parameters.get(3);
        String status=parameters.get(4);
        return changeStatusOfBug(personName,boardName,teamName,bugName,status);
    }

    private String changeStatusOfBug(String personName,String boardName, String teamName, String bugName, String status){

        if (!itemRepository.getMembers().containsKey(personName)) {
            throw new IllegalArgumentException(String.format(PERSON_NOT_EXISTS_ERROR_MESSAGE, personName));
        }
        if (!itemRepository.getBoard().containsKey(boardName)) {
            throw new IllegalArgumentException(String.format(BOARD_NOT_EXISTS_ERROR_MESSAGE, boardName));
        }
        if(!itemRepository.getTeams().containsKey(teamName)){
            throw new IllegalArgumentException(String.format(TEAM_NOT_EXISTS_ERROR_MESSAGE,teamName));
        }
        if(!itemRepository.getBugList().containsKey(bugName)){
            throw new IllegalArgumentException(String.format(NO_EXISTING_WORK_ITEM,bugName));
        }

        Members person=itemRepository.getMembers().get(personName);
        Boards board=itemRepository.getBoard().get(boardName);
        Teams team=itemRepository.getTeams().get(teamName);
        Bug bug=itemRepository.getBugList().get(bugName);

        if (!(team.getMembers().contains(person) && team.getBoards().contains(board))) {
            return String.format(NO_EXISTING_PERSON_OR_BOARD_IN_TEAM, personName, boardName, teamName);
        }

        if(!board.getListOfWorkingItems().contains(bug)){
            return String.format(NOT_EXISTING_ITEM_ERROR_MESSAGE,bugName);
        }

        BugStatusType statusType=BugStatusType.valueOf(status.toUpperCase());
        bug.setStatusType(statusType);
        person.addToHistory(String.format(ADD_TO_PERSON_HISTORY_CHANGESTATUS,bugName));
        board.addToHistory(String.format(ADD_TO_BOARD_HISTORY_CHANGESTATUS,personName,bugName));
        return String.format(SUCCESSFULLY_CHANGED_STATUS_OF_BUG,bugName);
    }
}
