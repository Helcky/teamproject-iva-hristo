package commands.change;
import commands.contracts.Command;
import core.contracts.ItemRepository;
import models.contracts.Boards;
import models.contracts.Feedback;
import models.contracts.Members;
import models.contracts.Teams;
import models.enums.FeedbackStatusType;

import static commands.CommandConstants.INVALID_NUMBER_OF_ARGUMENTS;
import static commands.change.ChangeCommandConstant.*;


import java.util.List;

public class ChangeStatusOfFeedback implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 5;

    private ItemRepository itemRepository;

    public ChangeStatusOfFeedback(ItemRepository itemRepository){
        this.itemRepository=itemRepository;
    }

    public String execute(List<String> parameters){
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(INVALID_NUMBER_OF_ARGUMENTS);
        }

        if(itemRepository.getFeedbackList().size()==0){
            throw new IllegalArgumentException(EMPTY_LIST_OF_FEEDBACK);
        }

        String personName=parameters.get(0);
        String boardName=parameters.get(1);
        String teamName=parameters.get(2);
        String feedbackName=parameters.get(3);
        String status=parameters.get(4);
        return changeStatusOfFeedback(personName,boardName,teamName,feedbackName,status);
    }

    private String changeStatusOfFeedback(String personName,String boardName,String teamName,String feedbackName, String status){

        if (!itemRepository.getMembers().containsKey(personName)) {
            throw new IllegalArgumentException(String.format(PERSON_NOT_EXISTS_ERROR_MESSAGE, personName));
        }
        if (!itemRepository.getBoard().containsKey(boardName)) {
            throw new IllegalArgumentException(String.format(BOARD_NOT_EXISTS_ERROR_MESSAGE, boardName));
        }
        if(!itemRepository.getTeams().containsKey(teamName)){
            throw new IllegalArgumentException(String.format(TEAM_NOT_EXISTS_ERROR_MESSAGE,teamName));
        }
        if(!itemRepository.getFeedbackList().containsKey(feedbackName)){
            throw new IllegalArgumentException(String.format(NO_EXISTING_WORK_ITEM,feedbackName));
        }

        Members person=itemRepository.getMembers().get(personName);
        Boards board=itemRepository.getBoard().get(boardName);
        Teams team=itemRepository.getTeams().get(teamName);
        Feedback feedback=itemRepository.getFeedbackList().get(feedbackName);

        if (!(team.getMembers().contains(person) && team.getBoards().contains(board))) {
            return String.format(NO_EXISTING_PERSON_OR_BOARD_IN_TEAM, personName, boardName, teamName);
        }

        if(!board.getListOfWorkingItems().contains(feedback)){
            return String.format(NOT_EXISTING_ITEM_ERROR_MESSAGE,feedbackName);
        }
        FeedbackStatusType statusType=FeedbackStatusType.valueOf(status.toUpperCase());

        feedback.setStatusType(statusType);
        person.addToHistory(String.format(ADD_TO_PERSON_HISTORY_CHANGESTATUS,feedbackName));
        board.addToHistory(String.format(ADD_TO_BOARD_HISTORY_CHANGESTATUS,personName,feedbackName));
        return String.format(SUCCESSFULLY_CHANGED_STATUS_OF_FEEDBACK,feedbackName);
    }

}
