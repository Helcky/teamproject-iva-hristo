package commands;

import commands.contracts.Command;
import core.contracts.ItemRepository;
import models.contracts.Members;

import java.util.List;

import static commands.CommandConstants.*;

public class ShowPeopleActivity implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    private final ItemRepository itemRepository;

    public ShowPeopleActivity(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(INVALID_NUMBER_OF_ARGUMENTS);
        }
        if (itemRepository.getMembers().size() == 0) {
            throw new IllegalArgumentException(NO_EXISTING_PEOPLE);
        }
        String personHistoryToShow = parameters.get(0);
        return showHistory(personHistoryToShow);
    }

    private String showHistory(String personHistoryToShow) {

        if (!itemRepository.getMembers().containsKey(personHistoryToShow)) {
            throw new IllegalArgumentException(String.format(PERSON_NOT_EXISTS_ERROR_MESSAGE, personHistoryToShow));
        }

        Members person = itemRepository.getMembers().get(personHistoryToShow);
        return person.printHistory();
    }

}
