package commands.list;

import commands.contracts.Command;
import core.contracts.ItemRepository;
import models.contracts.Story;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static commands.list.CommandListConstants.*;

public class SortWorkItemsBySize implements Command {

    private ItemRepository itemRepository;

    public SortWorkItemsBySize(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public String execute(List<String> parameters) {

        if (itemRepository.getStoryList().size() == 0) {
            throw new IllegalArgumentException(EMPTY_LIST_STORIES);
        }

        List<Story> listOfStoryValues = new ArrayList<>(itemRepository.getStoryList().values());

        List<Story> sortedList = listOfStoryValues.stream()
                .sorted(Comparator.comparing(Story::getSizeType))
                .collect(Collectors.toList());


        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("LIST OF STORY SORTED BY SIZE: %n"));
        for (Story story : sortedList) {
            stringBuilder.append(story.toString());
        }
        stringBuilder.append(System.lineSeparator());
        return stringBuilder.toString();
    }
}
