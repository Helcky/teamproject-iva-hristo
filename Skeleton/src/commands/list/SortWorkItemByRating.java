package commands.list;

import commands.contracts.Command;
import core.contracts.ItemRepository;
import models.contracts.Feedback;

import java.util.*;
import java.util.stream.Collectors;

import static commands.list.CommandListConstants.*;

public class SortWorkItemByRating implements Command {


    private ItemRepository itemRepository;

    public SortWorkItemByRating(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public String execute(List<String> parameters) {

        if (itemRepository.getFeedbackList().size() == 0) {
            throw new IllegalArgumentException(EMPTY_LIST_RATING);
        }

        List<Feedback> listOfFeedbackValues = new ArrayList<>(itemRepository.getFeedbackList().values());

        List<Feedback> sortedList = listOfFeedbackValues.stream()
                .sorted(((feedback1, feedback2) -> feedback1.getRating() - feedback2.getRating()))
                .collect(Collectors.toList());


        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("LIST OF FEEDBACK SORTED BY RATING: %n"));
        for (Feedback feedback : sortedList) {
            stringBuilder.append(feedback.toString());
        }
        stringBuilder.append(System.lineSeparator());
        return stringBuilder.toString();
    }
}
