package commands.list;

import commands.contracts.Command;
import core.contracts.ItemRepository;
import models.contracts.BugAndStory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static commands.CommandConstants.*;
import static commands.list.CommandListConstants.*;

public class FilterByAssignee implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;
    private ItemRepository itemRepository;

    public FilterByAssignee(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public String execute(List<String> parameters) {

        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(INVALID_NUMBER_OF_ARGUMENTS);
        }

        String person = parameters.get(0);
        return filterByAssignee(person);
    }

    private String filterByAssignee(String person) {

        if (!itemRepository.getMembers().containsKey(person)) {
            throw new IllegalArgumentException(String.format(PERSON_NOT_EXISTS_ERROR_MESSAGE, person));
        }

        List<BugAndStory> list = new ArrayList<>();
        list.addAll(itemRepository.getBugList().values());
        list.addAll(itemRepository.getStoryList().values());

        List<BugAndStory> listWithAssignee = list.stream()
                .filter(workitem -> workitem.getAssignee() != null)
                .collect(Collectors.toList());

        List<BugAndStory> result = listWithAssignee
                .stream()
                .filter(workItem -> (workItem.getAssignee().getName()).equals(person))
                .collect(Collectors.toList());

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("LIST OF WORK ITEMS WITH ASSIGNEE: %s%n", person));
        for (BugAndStory temp : result) {
            stringBuilder.append(temp.toString());
        }
        stringBuilder.append(System.lineSeparator());
        return stringBuilder.toString();
    }
}
