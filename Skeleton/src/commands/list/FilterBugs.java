package commands.list;

import commands.contracts.Command;
import core.contracts.ItemRepository;
import models.contracts.Bug;

import java.util.ArrayList;
import java.util.List;

import static commands.list.CommandListConstants.*;

public class FilterBugs implements Command {
    private final ItemRepository itemRepository;

    public FilterBugs(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    @Override
    public String execute(List<String> parameters) {

        if (itemRepository.getBugList().size() == 0) {
            throw new IllegalArgumentException(NO_EXISTING_BUG);
        }

        List<Bug> bugList = new ArrayList<>(itemRepository.getBugList().values());

        StringBuilder stringBuilder = new StringBuilder();
        for (Bug bug : bugList) {
            stringBuilder.append(bug.toString());
        }
        return stringBuilder.toString();
    }
}
