package commands.list;

import commands.contracts.Command;
import core.contracts.ItemRepository;
import models.contracts.WorkItems;

import java.util.*;
import java.util.stream.Collectors;

import static commands.list.CommandListConstants.*;

public class SortWorkItemsByTitle implements Command {

    private ItemRepository itemRepository;

    public SortWorkItemsByTitle(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public String execute(List<String> parameters) {

        if (itemRepository.getItems().size() == 0) {
            throw new IllegalArgumentException(EMPTY_LIST_WORK_ITEMS);
        }

        List<WorkItems> listOfValues = new ArrayList<>(itemRepository.getItems().values());

        List<WorkItems> sortedList = listOfValues.stream()
                .sorted((workitem1, workitem2) -> workitem1.getTitle().compareTo(workitem2.getTitle()))
                .collect(Collectors.toList());

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("LIST OF WORK ITEMS SORTED BY TITLE: %n"));
        for (WorkItems tempWorkItem : sortedList) {
            stringBuilder.append(tempWorkItem.toString());
        }
        stringBuilder.append(System.lineSeparator());
        return stringBuilder.toString();
    }
}










