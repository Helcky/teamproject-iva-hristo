package commands.list;

import commands.contracts.Command;
import core.contracts.ItemRepository;
import models.contracts.WorkItems;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static commands.CommandConstants.*;

public class FilterByStatus implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    private ItemRepository itemRepository;

    public FilterByStatus(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public String execute(List<String> parameters) {

        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(INVALID_NUMBER_OF_ARGUMENTS);
        }
        if (itemRepository.getItems().size() == 0) {
            throw new IllegalArgumentException("Empty list with work items");
        }

        String status = parameters.get(0);
        return filterByStatus(status);
    }

    private String filterByStatus(String status) {
        AllStatusEnum statusType = AllStatusEnum.valueOf(status.toUpperCase());

        List<WorkItems> list = new ArrayList<>(itemRepository.getItems().values());

        List<WorkItems> result = list
                .stream()
                .filter(workItem -> workItem.getGlobalStatus().equals(statusType))
                .collect(Collectors.toList());

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("LIST OF WORK ITEMS WITH STATUS: %s%n", status));
        for (WorkItems temp : result) {
            stringBuilder.append(temp.toString());
        }
        stringBuilder.append(System.lineSeparator());
        return stringBuilder.toString();
    }
}
