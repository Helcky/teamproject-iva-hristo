package commands;

import commands.contracts.Command;
import core.contracts.ItemRepository;
import models.CommentImpl;
import models.contracts.*;

import static commands.CommandConstants.*;

import java.util.List;


public class AddCommentToWorkItem implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 5;

    private final ItemRepository itemRepository;


    public AddCommentToWorkItem(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public String execute(List<String> parameters) {

        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(INVALID_NUMBER_OF_ARGUMENTS);
        }

        String personName = parameters.get(0);
        String boardName = parameters.get(1);
        String teamName = parameters.get(2);
        String workItemName = parameters.get(3);
        String message = parameters.get(4);

        return addCommentToWorkItem(personName, boardName, teamName, workItemName, message);
    }

    private String addCommentToWorkItem(String personName, String boardName, String teamName, String workItemName, String message) {


        if (!itemRepository.getMembers().containsKey(personName)) {
            throw new IllegalArgumentException(String.format(PERSON_NOT_EXISTS_ERROR_MESSAGE, personName));
        }
        if (!itemRepository.getBoard().containsKey(boardName)) {
            throw new IllegalArgumentException(String.format(BOARD_NOT_EXISTS_ERROR_MESSAGE, boardName));
        }
        if (!itemRepository.getTeams().containsKey(teamName)) {
            throw new IllegalArgumentException(String.format(TEAM_NOT_EXISTS_ERROR_MESSAGE, teamName));
        }
        if (!itemRepository.getItems().containsKey(workItemName)) {
            throw new IllegalArgumentException(String.format(NOT_EXISTING_ITEM_ERROR_MESSAGE, workItemName));
        }

        Members person = itemRepository.getMembers().get(personName);
        Boards board = itemRepository.getBoard().get(boardName);
        Teams team = itemRepository.getTeams().get(teamName);
        WorkItems item = itemRepository.getItems().get(workItemName);

        if (!(team.getMembers().contains(person) && team.getBoards().contains(board))) {
            return String.format(NO_EXISTING_PERSON_OR_BOARD_IN_TEAM, personName, boardName, teamName);
        }

        if (!board.getListOfWorkingItems().contains(item)) {
            return String.format(NOT_EXISTING_ITEM_ERROR_MESSAGE, workItemName);
        }

        Comment comment = new CommentImpl(person, message);
        item.addCommentToWorkItem(comment);

        person.addToHistory(String.format(ADDED_COMMENT_TO_WORKITEM, message));
        board.addToHistory(String.format(ADDED_COMMENT_TO_WORKITEM_IN_BOARD, personName, message, boardName));
        return String.format(COMMENT_ADDED_SUCCESS_MESSAGE, personName, workItemName);
    }


}

