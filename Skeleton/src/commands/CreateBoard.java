package commands;

import commands.contracts.Command;
import core.contracts.ItemRepository;
import core.contracts.WIMFactory;
import models.contracts.Teams;
import models.contracts.Boards;

import java.util.List;

import static commands.CommandConstants.*;

public class CreateBoard implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

    private final ItemRepository itemRepository;
    private final WIMFactory wimFactory;

    public CreateBoard(ItemRepository itemRepository, WIMFactory wimFactory) {
        this.itemRepository = itemRepository;
        this.wimFactory = wimFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(INVALID_NUMBER_OF_ARGUMENTS);
        }
        String teamName = parameters.get(0);
        String boardName = parameters.get(1);
        return createBoard(teamName, boardName);
    }

    private String createBoard(String teamName, String boardName) {

        if (!itemRepository.getTeams().containsKey(teamName)) {
            throw new IllegalArgumentException(String.format(TEAM_NOT_EXISTS_ERROR_MESSAGE, teamName));
        }

        Teams team = itemRepository.getTeams().get(teamName);

        if (team.getBoards()
                .stream()
                .anyMatch(boards -> boards.getName().equals(boardName))) {
            return String.format(BOARD_EXISTS_ERROR_MESSAGE, boardName);
        }

        Boards board = wimFactory.createBoard(boardName);
        itemRepository.addBoards(boardName, board);

        team.addBoardToTeam(board);
        board.addToHistory(String.format(ADD_BOARD_TO_HISTORY, boardName, teamName));
        return String.format(BOARD_CREATED_SUCCESS_MESSAGE, boardName);
    }
}
