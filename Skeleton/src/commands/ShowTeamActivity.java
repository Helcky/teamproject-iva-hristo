package commands;


import commands.contracts.Command;
import core.contracts.ItemRepository;
import models.contracts.Teams;

import java.util.List;
import java.util.stream.Collectors;

import static commands.CommandConstants.*;

public class ShowTeamActivity implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    private final ItemRepository itemRepository;

    public ShowTeamActivity(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(INVALID_NUMBER_OF_ARGUMENTS);
        }
        if (itemRepository.getTeams().size() == 0) {
            throw new IllegalArgumentException(NO_EXISTING_TEAM);
        }
        String teamName = parameters.get(0);
        return showHistory(teamName);
    }

    private String showHistory(String teamName) {

        if (!itemRepository.getTeams().containsKey(teamName)) {
            throw new IllegalArgumentException(String.format(TEAM_NOT_EXISTS_ERROR_MESSAGE, teamName));
        }

        Teams team = itemRepository.getTeams().get(teamName);
        List<String> random = team.getBoards()
                .stream()
                .flatMap(boards -> boards.getHistory().stream())
                .collect(Collectors.toList());

        return team.printHistory();
    }


}
