package commands;

import commands.contracts.Command;
import core.contracts.ItemRepository;
import models.contracts.*;

import java.util.List;

import static commands.CommandConstants.*;

public class UnassignWorkItemToPerson implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 4;
    private ItemRepository itemRepository;

    public UnassignWorkItemToPerson(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(INVALID_NUMBER_OF_ARGUMENTS);
        }

        String personName = parameters.get(0);
        String boardName = parameters.get(1);
        String teamName = parameters.get(2);
        String workItemName = parameters.get(3);
        return unassignWorkItemToPerson(personName, boardName, teamName, workItemName);
    }

    private String unassignWorkItemToPerson(String personName, String boardName, String teamName, String workItemName) {

        if (!itemRepository.getMembers().containsKey(personName)) {
            throw new IllegalArgumentException(String.format(PERSON_NOT_EXISTS_ERROR_MESSAGE, personName));
        }

        if (!itemRepository.getBoard().containsKey(boardName)) {
            throw new IllegalArgumentException(String.format(BOARD_NOT_EXISTS_ERROR_MESSAGE, boardName));
        }
        if (!itemRepository.getTeams().containsKey(teamName)) {
            throw new IllegalArgumentException(String.format(TEAM_NOT_EXISTS_ERROR_MESSAGE, teamName));
        }

        Members person = itemRepository.getMembers().get(personName);
        Boards board = itemRepository.getBoard().get(boardName);
        Teams team = itemRepository.getTeams().get(teamName);
        WorkItems workItem = itemRepository.getItems().get(workItemName);

        if (!(team.getMembers().contains(person) && team.getBoards().contains(board))) {
            return String.format(NO_EXISTING_PERSON_OR_BOARD_IN_TEAM, personName, boardName, teamName);
        }

        switch (isInstance(workItemName)) {
            case 1:
                Bug bug = itemRepository.getBugList().get(workItemName);
                bug.setAssignee(null);
                person.removeWorkItem(workItem);
                person.addToHistory(String.format(UNASSIGNED_WORKITEM, workItemName));
                board.addToHistory(String.format(UNASSIGNED_WORK_ITEM_TO_BOARD, workItemName, personName));
                return String.format(SUCCESSFULLY_UNASSIGNED_WORK_ITEM, workItemName, personName);
            case 2:
                Story story = itemRepository.getStoryList().get(workItemName);
                story.setAssignee(null);
                person.removeWorkItem(workItem);
                person.addToHistory(String.format(UNASSIGNED_WORKITEM, workItemName));
                board.addToHistory(String.format(UNASSIGNED_WORK_ITEM_TO_BOARD, workItemName, personName));
                return String.format(SUCCESSFULLY_UNASSIGNED_WORK_ITEM, workItemName, personName);
            default:
                return FAILED_UNASSIGNEE;
        }
    }

    private int isInstance(String nameToCheck) {

        if (itemRepository.getBugList().containsKey(nameToCheck)) {
            return 1;
        } else if (itemRepository.getStoryList().containsKey(nameToCheck)) {
            return 2;
        } else {
            throw new IllegalArgumentException(String.format(NOT_EXISTING_ITEM_ERROR_MESSAGE, nameToCheck));
        }
    }

}
