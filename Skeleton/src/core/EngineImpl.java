package core;

import core.contracts.*;
import core.factories.CommandFactoryImpl;
import core.factories.WIMFactoryImpl;
import core.providers.CommandParseImpl;
import commands.contracts.Command;
import core.providers.ConsoleReader;
import core.providers.ConsoleWriter;

import java.util.List;

public class EngineImpl implements Engine {
    private static final String TERMINATION_COMMAND = "Exit";

    private WIMFactory wimFactory;
    private CommandFactory commandFactory;
    private ItemRepository itemRepository;
    private CommandParser commandParser;
    private Writer writer;
    private Reader reader;

    public EngineImpl() {
        wimFactory = new WIMFactoryImpl();
        commandFactory = new CommandFactoryImpl();
        itemRepository = new ItemRepositoryImpl();
        commandParser = new CommandParseImpl();
        writer = new ConsoleWriter();
        reader = new ConsoleReader();
    }

    @Override
    public void start() {
        while (true){
            try{
                String commandAsString = reader.readLine();
                if (commandAsString.equalsIgnoreCase(TERMINATION_COMMAND)){
                    break;
                }
                processCommand(commandAsString);
            }catch (Exception exception){
                writer.writeLine(exception.getMessage() != null && !exception.getMessage().isEmpty() ? exception.getMessage() : exception.toString());
            }
        }
    }

    private void processCommand (String commandAsString) {
        if (commandAsString == null || commandAsString.trim().equals("")){
            throw new IllegalArgumentException("Command cannot be empty or null.");
        }
        String commandName = commandParser.parseCommand(commandAsString);
        Command command = commandFactory.createCommand(commandName,wimFactory,itemRepository);
        List<String> parameters = commandParser.parseParameters(commandAsString);
        String executionResult = command.execute(parameters);
        writer.writeLine(executionResult);
    }
}
