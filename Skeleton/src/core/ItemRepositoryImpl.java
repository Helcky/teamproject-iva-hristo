package core;

import core.contracts.ItemRepository;
import models.contracts.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ItemRepositoryImpl implements ItemRepository {
    private Map<String,WorkItems> items;
    private Map<String,Teams> team;
    private Map<String,Boards> board;
    private Map<String,Members> member;
    private Map<String,Bug> bugList;
    private Map<String,Feedback> feedbackList;
    private Map<String,Story> storyList;

    public ItemRepositoryImpl() {
        this.items = new HashMap<>();
        this.team = new HashMap<>();
        this.board = new HashMap<>();
        this.member = new HashMap<>();
        this.bugList=new HashMap<>();
        this.feedbackList=new HashMap<>();
        this.storyList=new HashMap<>();
    }

    @Override
    public Map<String, WorkItems> getItems() {
        return new HashMap<>(items);
    }

    @Override
    public Map<String, Teams> getTeams() {
        return new HashMap<>(team);
    }

    @Override
    public Map<String, Boards> getBoard() {
        return new HashMap<>(board);
    }

    @Override
    public Map<String, Members> getMembers() {
        return new HashMap<>(member);
    }

    public Map<String,Bug> getBugList(){return new HashMap<>(bugList);}

    public Map<String,Feedback> getFeedbackList(){return new HashMap<>(feedbackList);}

    public Map<String,Story> getStoryList(){return new HashMap<>(storyList);}

    @Override
    public void addItems(String name, WorkItems item) {
        this.items.put(name,item);
    }

    @Override
    public void addTeams(String name, Teams team) {
        this.team.put(name,team);
    }

    @Override
    public void addBoards(String name, Boards board) {
        this.board.put(name,board);
    }

    @Override
    public void addMembers(String name, Members member) {
        this.member.put(name,member);
    }

    public void addBug(String name, Bug bug){this.bugList.put(name,bug);}

    public void addFeedback(String name,Feedback feedback){ this.feedbackList.put(name,feedback);}

    public void addStory(String name,Story story){this.storyList.put(name,story);}

}
