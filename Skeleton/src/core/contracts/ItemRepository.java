package core.contracts;

import models.contracts.*;

import java.util.List;
import java.util.Map;


public interface ItemRepository {
    Map<String, WorkItems> getItems();
    Map<String, Teams> getTeams();
    Map<String, Boards> getBoard();
    Map<String, Members> getMembers();
    Map<String,Bug> getBugList();
    Map<String,Feedback> getFeedbackList();
    Map<String,Story> getStoryList();

    void addItems(String name,WorkItems item);
    void addTeams(String name,Teams team);
    void addBoards(String name,Boards board);
    void addMembers(String name, Members member);
    void addBug(String name,Bug bug);
    void addFeedback(String name,Feedback feedback);
    void addStory(String name, Story story);
}
