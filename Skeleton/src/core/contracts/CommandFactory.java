package core.contracts;

import commands.contracts.Command;

public interface CommandFactory {
    Command createCommand(String commandTypeAsString, WIMFactory wimFactory, ItemRepository itemRepository);
}
