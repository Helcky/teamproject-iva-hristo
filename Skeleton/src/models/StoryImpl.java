package models;

import commands.list.AllStatusEnum;
import models.contracts.Members;
import models.contracts.Story;
import models.enums.PriorityType;
import models.enums.SizeType;
import models.enums.StoryStatusType;

public class StoryImpl extends WorkItemsImpl implements Story {

    private StoryStatusType status;
    private PriorityType priority;
    private Members assignee;
    private SizeType size;

    public StoryImpl(String title, String description) {
        super(title, description);
        this.status = StoryStatusType.NOTDONE;
        this.priority = PriorityType.MINOR;
        this.size = SizeType.SMALL;
    }

    public void setPriorityType(PriorityType priority) {
        this.priority = priority;
    }

    public void setAssignee(Members assignee) {
        //ValidationHelper.checkNull(assignee);
        this.assignee = assignee;
    }

    public void setSize(SizeType size) {
        this.size = size;
    }

    public void setStatus(StoryStatusType status) {
        this.status = status;
    }

    public PriorityType getPriorityType() {
        return priority;
    }

    public Members getAssignee() {
        return assignee;
    }

    public SizeType getSizeType() {
        return size;
    }

    public StoryStatusType getStatusType() {
        return status;
    }

    public AllStatusEnum getGlobalStatus() {
        return AllStatusEnum.valueOf(getStatusType().toString().toUpperCase().replaceAll("\\s", ""));
    }

    public String toString() {
        return String.format("%s, Status: %s, Priority: %s, Assignee: %s, Size: %s%n",
                super.toString(), getStatusType(), getPriorityType(), getAssignee(), getSizeType());
    }

}
