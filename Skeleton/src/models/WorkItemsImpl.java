package models;

import commands.list.AllStatusEnum;
import core.contracts.ItemRepository;
import models.contracts.Comment;
import models.contracts.Members;
import models.contracts.WorkItems;

import java.util.ArrayList;
import java.util.List;

public abstract class WorkItemsImpl implements WorkItems {
    private static long staticID=0;

    private long ID;
    private String title;
    private String description;
    private List<Comment> comments;
    private List<String> history;

    public WorkItemsImpl(String title, String description) {
        setTitle(title);
        setDescription(description);
        comments = new ArrayList<>();
        history = new ArrayList<>();
        ID=++staticID;
    }

    public long getId() {
        return ID;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }

    public List<String> getHistory() {
        return new ArrayList<>(history);
    }

    public abstract AllStatusEnum getGlobalStatus();

    private void setTitle(String title) {
        ValidationHelper.checkTitle(title);
        this.title = title;
    }

    private void setDescription(String description) {
        ValidationHelper.checkDescription(description);
        this.description = description;
    }

    public void addCommentToWorkItem(Comment comment){
        comments.add(comment);
    }

    @Override
    public String toString() {
        return String.format("Id: %d, Title: %s, Description: %s",getId(),getTitle(),getDescription());
    }
}
