package models.contracts;

import models.WorkItemsImpl;

import java.util.List;

public interface Boards {
    List<WorkItems> getListOfWorkingItems();
    List<String> getHistory();
    String getName();
    void addWorkItem(WorkItems item);
    void addToHistory(String activity);
    String printHistory();
    String historyToString();
}
