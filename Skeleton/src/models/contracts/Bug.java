package models.contracts;

import models.enums.BugStatusType;
import models.enums.PriorityType;
import models.enums.SeverityType;

import java.util.List;

public interface Bug extends WorkItems,BugAndStory {
    List<String> getStepToReproduce();
    SeverityType getSeverityType();
    BugStatusType getStatusType();
    void setSeverity(SeverityType severityType);
    void setStatusType(BugStatusType status);
    void setAssignee(Members members);
    void setStep(String step);
}
