package models.contracts;

import models.enums.FeedbackStatusType;

public interface Feedback extends WorkItems {
    int getRating();
    FeedbackStatusType getStatusType();
    void setRating(int rating);
    void setStatusType(FeedbackStatusType status);
}
