package models.contracts;

import models.WorkItemsImpl;

import java.util.List;

public interface Members  {
    List<WorkItems> getListOfWorkingItems();
    List<String> getHistory();
    String getName();
    void addToHistory(String activity);
    String printHistory();
    void addWorkItem(WorkItems workItem);
    void removeWorkItem(WorkItems workItem);
}
