package models;

import models.contracts.Boards;
import models.contracts.WorkItems;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class BoardsImpl implements Boards {

    private String name;
    private List<WorkItems> workItems;
    private List<String> history;

    public BoardsImpl(String name) {
        setName(name);
        workItems = new ArrayList<>();
        history = new ArrayList<>();

    }

    public List<WorkItems> getListOfWorkingItems() {
        return new ArrayList<>(workItems);
    }

    public List<String> getHistory() {
        return new ArrayList<>(history);
    }

    public String getName() {
        return name;
    }

    public void addWorkItem(WorkItems item) {
        ValidationHelper.checkNull(item);
        workItems.add(item);
    }

    public void addToHistory(String activity) {
        ValidationHelper.checkNull(activity);
        history.add(activity);
    }

    @Override
    public String toString() {
        return String.format("Board name: %s", getName());
    }

    public String historyToString() {
        return String.format("History of: %s%n %s", name, printHistory());
    }

    public String printHistory() {
        if (history.size() == 0) {
            return String.format("History: %s%n" +
                    "No activity in this history", name);
        }
        LocalDateTime dateNTime = LocalDateTime.now();
        DateTimeFormatter myFormatDateNTime = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        String formattedDate = dateNTime.format(myFormatDateNTime);
        StringBuilder stringBuilder = new StringBuilder();
        for (String activity : history) {
            stringBuilder.append(formattedDate + ": ");
            stringBuilder.append(activity);
            stringBuilder.append(System.getProperty("line.separator"));
        }
        return stringBuilder.toString();
    }

    private void setName(String name) {
        ValidationHelper.checkFunctionalName(name);
        this.name = name;
    }

}
