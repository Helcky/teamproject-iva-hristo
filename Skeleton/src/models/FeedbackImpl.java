package models;

import commands.list.AllStatusEnum;
import models.contracts.Feedback;
import models.enums.FeedbackStatusType;

public class FeedbackImpl extends WorkItemsImpl implements Feedback {

    private FeedbackStatusType status;
    private int rating;

    public FeedbackImpl(String title, String description,int rating) {
        super(title, description);
        this.status=FeedbackStatusType.NEW;
        setRating(rating);
    }

    public int getRating() {
        return rating;
    }
    public FeedbackStatusType getStatusType() {return status;}

    public void setRating(int rating) {
        ValidationHelper.checkRating(rating);
        this.rating = rating;
    }

    public void setStatusType(FeedbackStatusType status){ this.status=status;};

    public AllStatusEnum getGlobalStatus(){
        return  AllStatusEnum.valueOf(getStatusType().toString().toUpperCase().replaceAll("\\s", ""));
    }

    public String toString(){
        return String.format("%s, Rating: %s,Status: %s%n",
                super.toString(),getRating(),getStatusType());
    }
}
